// Boss.h
#ifndef BOSS_H

#define BOSS_H

#include <string>

class Boss {
private:
    int health;
public:
    // Constructor
    Boss();

    
    void StartFight();

   
    bool IsDefeated() const;
};

#endif
