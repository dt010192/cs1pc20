#include <iostream>
#include <string>
#include <vector>
#include "Character.h"
#include "Player.h"
#include "Room.h"
#include "Item.h"
#include "Chest.h" // Include Chest.h for the Chest class
#include "Boss.h" // Include Boss.h for the Boss class

int main() {
    Room startingRoom("Starting Room", "You are in the starting room. There is a Sword lying on the ground.");
    Room saltySprings("Salty Springs", "You are in Salty Springs. There is a Key on the table.");
    Room pleasantPark("Pleasant Park", "You are in Pleasant Park. There is a Potion in the corner.");
    Room retailRow("Retail Row", "You are in Retail Row. There is a Map on the wall.");

    // Add items to rooms
    Item sword("Sword", "A sharp Sword for battle.");
    Item key("Key", "A shiny Key that looks important.");
    Item potion("Potion", "A healing Potion to restore health.");
    Item map("Map", "A Map to help you navigate the area.");

    startingRoom.AddItem(sword);
    saltySprings.AddItem(key);
    pleasantPark.AddItem(potion);
    retailRow.AddItem(map);

    // Connect rooms
    startingRoom.AddExit("Salty Springs", &saltySprings);
    startingRoom.AddExit("Pleasant Park", &pleasantPark);
    startingRoom.AddExit("Retail Row", &retailRow);
    saltySprings.AddExit("Starting Room", &startingRoom);
    pleasantPark.AddExit("Starting Room", &startingRoom);
    retailRow.AddExit("Starting Room", &startingRoom);

    // Create a player
    Player player("Haseeb", 100);
    player.SetLocation(&startingRoom); // Set the player's starting room

    // Game loop
    int roomsVisited = 0;
    Boss boss;

    while (true) {
        // Display player information and current room description
        std::cout << "Player Name: " << player.GetName() << "\n";
        std::cout << "Player Health: " << player.GetHealth() << "\n";
        std::cout << "Current Location: " << player.GetLocation()->GetName() << "\n";
        std::cout << "Description: " << player.GetLocation()->GetDescription() << "\n\n";

        // Display player's inventory
        player.DisplayInventory();

        // Interact with items in the room or quit
        std::cout << "Do you want to interact with the item in this room, the chest, or quit? (item/chest/quit): ";
        std::string interactionChoice;
        std::cin >> interactionChoice;

        if (interactionChoice == "item") {
            if (!player.InventoryIsFull()) {
                std::cout << "Enter the name of the item you want to interact with: ";
                std::string itemName;
                std::cin >> itemName;
                player.GetLocation()->InteractWithItem(itemName, player);
            } else {
                std::cout << "Your inventory is full. Do you want to swap an item? (yes/no): ";
                std::string swapChoice;
                std::cin >> swapChoice;
                if (swapChoice == "yes") {
                    player.DisplayInventory();
                    std::cout << "Enter the name of the item you want to drop: ";
                    std::string dropItemName;
                    std::cin >> dropItemName;
                    player.InteractWithInventoryItem(dropItemName); // Simulate dropping item
                    std::cout << "Enter the name of the item you want to pick up: ";
                    std::string newItemName;
                    std::cin >> newItemName;
                    player.GetLocation()->InteractWithItem(newItemName, player);
                } else {
                    std::cout << "You chose not to swap items.\n";
                }
            }
        } else if (interactionChoice == "chest") {
            player.GetLocation()->InteractWithChest(player);
        } else if (interactionChoice == "quit") {
            std::cout << "Quitting the game...\n";
            break;
        } else {
            std::cout << "Invalid choice. Please enter 'item', 'chest', or 'quit'.\n\n";
        }

        // Display descriptions of other rooms
        player.GetLocation()->PrintAvailableExits();

        // Prompt to choose the next room or quit
        std::cout << "Enter the number of the room you want to enter (or 0 to stay) or 'quit' to exit the game: ";
        std::string input;
        std::cin >> input;
        if (input == "quit") {
            std::cout << "Quitting the game...\n";
            break;
        }
        int choice = std::stoi(input);

        // Move player to the next room
        if (choice >= 1 && choice <= player.GetLocation()->GetExits().size()) {
            auto it = player.GetLocation()->GetExits().begin();
            std::advance(it, choice - 1);
            player.SetLocation(it->second);
            std::cout << "\n"; // Leave a line when entering a new room
        } else if (choice == 0) {
            // Player stays in the current room
            std::cout << "\n"; // Leave a line to separate player choices
        } else {
            std::cout << "Invalid choice. Staying in the current room.\n\n";
        }

        // Increment rooms visited
        roomsVisited++;

        if (roomsVisited == 4) {
            boss.StartFight();
            if (boss.IsDefeated()) {
                std::cout << "Congratulations! You defeated the boss and won the game!\n";
                break; // Exit the game loop after defeating the boss
            } else {
                std::cout << "Game over. The boss defeated you. Better luck next time!\n";
                break; // Exit the game loop after being defeated by the boss
            }
        }
    }

    return 0;
}
