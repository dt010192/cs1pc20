// Character.cpp
#include "Character.h"

// Constructor
Character::Character(const std::string& charName, int charHealth) : name(charName), health(charHealth) {}


void Character::TakeDamage(int damage) {
    health -= damage;
    if (health < 0) {
        health = 0;
    }
}


const std::string& Character::GetName() const {
    return name;
}

int Character::GetHealth() const {
    return health;
}

const std::vector<Item>& Character::GetInventory() const {
    return inventory;
}
