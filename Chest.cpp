#include <algorithm>
#include "Chest.h"

// Constructor
Chest::Chest() {
    
    items.push_back(Item("HealthPotion", "Health"));
    items.push_back(Item("ManaPotion", "Mana"));
    items.push_back(Item("Dagger", "Weapon"));
    items.push_back(Item("Scroll", "Spell"));
}


void Chest::AddItem(const Item& item) {
    items.push_back(item);
}


void Chest::RemoveItem(const Item& item) {
    items.erase(std::remove(items.begin(), items.end(), item), items.end());
}


bool Chest::HasItem(const Item& item) const {
    return std::find(items.begin(), items.end(), item) != items.end();
}


const std::vector<Item>& Chest::GetItems() const {
    return items;
}



void Chest::Clear() {
    items.clear();
}


