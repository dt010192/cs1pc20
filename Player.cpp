// Player.cpp
#include "Player.h"
#include <iostream>
#include <algorithm>

// Constructor
Player::Player(const std::string& playerName, int playerHealth) : Character(playerName, playerHealth), location(nullptr), inventorySize(0) {}

// Function to set player's location
void Player::SetLocation(Room* newLocation) {
    location = newLocation;
}

// Function to get player's location
Room* Player::GetLocation() const {
    return location;
}

// Function to add an item to the player's inventory
bool Player::AddToInventory(const Item& item) {
    if (inventorySize < 2) {
        inventory.push_back(item);
        inventorySize++;
        return true; // Indicate success
    } else {
        std::cout << "Your inventory is full. Do you want to swap an item? (yes/no): ";
        std::string choice;
        std::cin >> choice;
        if (choice == "yes") {
            DisplayInventory();
            std::cout << "Enter the name of the item you want to drop from your inventory: ";
            std::string dropItemName;
            std::cin >> dropItemName;
            RemoveFromInventory(Item(dropItemName, "")); // Remove the specified item
            inventory.push_back(item); // Add the new item from the chest
            std::cout << "You swapped " << dropItemName << " with " << item.GetName() << " from the chest.\n";
            return true;
        } else {
            std::cout << "You chose not to swap items.\n";
            return false;
        }
    }
}


// Function to remove an item from the player's inventory
void Player::RemoveFromInventory(const Item& item) {
    inventory.erase(std::remove(inventory.begin(), inventory.end(), item), inventory.end());
    inventorySize--;
}

// Function to interact with an item in the player's inventory
void Player::InteractWithInventoryItem(const std::string& itemName) {
    for (auto& item : inventory) {
        if (item.GetName() == itemName) {
            std::cout << "You interact with the " << itemName << std::endl;
            return;
        }
    }
    std::cout << "There is no " << itemName << " in your inventory." << std::endl;
}

// Function to display player's inventory
void Player::DisplayInventory() const {
    std::cout << "Inventory: ";
    for (const auto& item : inventory) {
        std::cout << item.GetName() << " ";
    }
    std::cout << std::endl;
}

// Function to get the player's inventory
const std::vector<Item>& Player::GetInventory() const {
    return inventory;
}

// Function to check if the player's inventory is full
bool Player::InventoryIsFull() const {
    return inventory.size() >= 2;
}
