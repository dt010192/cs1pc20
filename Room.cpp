#include "Room.h"
#include "Item.h"
#include <algorithm>
#include <iostream>
#include <random> // Include <random> for random number generation

// Constructor
Room::Room(const std::string& name, const std::string& desc) : name(name), description(desc) {}

// Function to add an item to the room
void Room::AddItem(const Item& item) {
    items.push_back(item);
}

// Function to remove an item from the room
void Room::RemoveItem(const Item& item) {
    items.erase(std::remove(items.begin(), items.end(), item), items.end());
}

// Function to add an exit to another room
void Room::AddExit(const std::string& direction, Room* room) {
    exits[direction] = room;
}

// Function to get the room's name
const std::string& Room::GetName() const {
    return name;
}

// Function to get the room's description
const std::string& Room::GetDescription() const {
    return description;
}

// Implement the InteractWithItem function
void Room::InteractWithItem(const std::string& itemName, Player& player) {
    for (auto& item : items) {
        if (item.GetName() == itemName) {
            std::cout << "You interact with the " << itemName << std::endl;
            // Assuming here that the interaction should involve adding the item to the player's inventory
            player.AddToInventory(item);
            return;
        }
    }
    std::cout << "There is no " << itemName << " in the room." << std::endl;
}

// Function to interact with the chest in the room
void Room::InteractWithChest(Player& player) {
    // Display chest contents
    std::cout << "You found a chest containing:\n";
    const std::vector<Item>& chestItems = chest.GetItems();
    for (const auto& item : chestItems) {
        std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
    }

    // Allow the player to interact with the chest
    std::cout << "Do you want to loot the chest? (yes/no): ";
    std::string choice;
    std::cin >> choice;
    if (choice == "yes") {
        // Check if player's inventory is full
        if (player.InventoryIsFull()) {
            std::cout << "Your inventory is full. Do you want to swap an item? (yes/no): ";
            std::cin >> choice;
            if (choice == "yes") {
                player.DisplayInventory();
                std::cout << "Enter the name of the item you want to drop from your inventory: ";
                std::string dropItemName;
                std::cin >> dropItemName;
                player.RemoveFromInventory(Item(dropItemName, "")); // Remove the specified item
            } else {
                std::cout << "You chose not to swap items.\n";
                return;
            }
        }

        // Randomly select an item from the chest
        if (!chestItems.empty()) {
            std::random_device rd;
            std::mt19937 gen(rd());
            std::uniform_int_distribution<> dis(0, chestItems.size() - 1);
            int randomIndex = dis(gen);
            const Item& selectedItem = chestItems[randomIndex];

            // Check if the player already has the selected item
            if (std::find(player.GetInventory().begin(), player.GetInventory().end(), selectedItem) != player.GetInventory().end()) {
                std::cout << "You already have " << selectedItem.GetName() << " in your inventory.\n";
                return;
            }

            player.AddToInventory(selectedItem);
            std::cout << "You looted " << selectedItem.GetName() << " from the chest!\n";

            // Remove the looted item from the chest
            chest.RemoveItem(selectedItem);
        } else {
            std::cout << "The chest is empty.\n";
        }
    } else {
        std::cout << "You chose not to loot the chest.\n";
    }
}


// Function to get an exit
Room* Room::GetExit(const std::string& direction) {
    auto it = exits.find(direction);
    if (it != exits.end()) {
        return it->second;
    } else {
        return nullptr;
    }
}

// Function to print available exits
void Room::PrintAvailableExits() const {
    std::cout << "Rooms you can enter:" << std::endl;
    int roomNumber = 1;
    for (const auto& exit : exits) {
        std::cout << roomNumber << ". " << exit.first << ": " << exit.second->GetDescription() << std::endl;
        roomNumber++;
    }
    std::cout << "0. Stay in " << name << std::endl;
}

// Getters for items and exits
const std::vector<Item>& Room::GetItems() const {
    return items;
}

const std::map<std::string, Room*>& Room::GetExits() const {
    return exits;
}
