// Boss.cpp
#include "Boss.h"
#include <iostream>

// Constructor
Boss::Boss() : health(100) {}


void Boss::StartFight() {
    std::cout << "You encounter the boss!\n";
    std::cout << "Answer the following questions correctly to defeat the boss.\n";


    int answer;
    std::cout << "What is 2 + 2? ";
    std::cin >> answer;

    if (answer == 4) {
        std::cout << "Correct! You defeated the boss!\n";
    } else {
        std::cout << "Incorrect! The boss defeated you!\n";
    }
}


bool Boss::IsDefeated() const {
    return health <= 0;
}
