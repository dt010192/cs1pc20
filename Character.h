// Character.h
#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>
#include <vector>
#include "Item.h"

class Character {
private:
    std::string name;
    int health;
    std::vector<Item> inventory;

public:
    // Constructor
    Character(const std::string& name, int health);

    // Function to take damage
    virtual void TakeDamage(int damage);

    // Getters
    const std::string& GetName() const;
    int GetHealth() const;
    const std::vector<Item>& GetInventory() const;
};

#endif
