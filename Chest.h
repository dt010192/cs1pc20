#ifndef CHEST_H
#define CHEST_H

#include <vector>
#include "Item.h"

class Chest {
private:
    std::vector<Item> items;

public:
    // Constructor
    Chest();

    // Function to add an item to the chest
    void AddItem(const Item& item);

   // Function to clear the chest contents
   void Clear();

    // Function to remove an item from the chest
    void RemoveItem(const Item& item);

    // Function to check if the chest contains a specific item
    bool HasItem(const Item& item) const;

    // Function to get all items in the chest
    const std::vector<Item>& GetItems() const;
};

#endif
