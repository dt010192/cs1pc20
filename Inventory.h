#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>
#include "Item.h"

class Inventory {
private:
    std::vector<Item> items;

public:
    // Constructor
    Inventory();

    // Function to add an item to the inventory
    void AddItem(const Item& item);

    // Function to remove an item from the inventory
    void RemoveItem(const Item& item);

    // Function to check if the inventory contains a specific item
    bool HasItem(const Item& item) const;

    // Function to get all items in the inventory
    const std::vector<Item>& GetItems() const;
};

#endif
