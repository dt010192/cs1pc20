// Item.cpp
#include "Item.h"
#include <iostream>

// Constructor
Item::Item(const std::string& itemName, const std::string& itemDescription)
    : name(itemName), description(itemDescription) {}

// Function to describe the interaction with the item
void Item::Interact() const {
    std::cout << "You interact with the " << name << ": " << description << std::endl;
}

// Getter for description
const std::string& Item::GetDescription() const {
    return description;
}

// Getter for name
const std::string& Item::GetName() const {
    return name;
}

// Overloading == operator to compare items
bool Item::operator==(const Item& other) const {
    return name == other.name;
}
