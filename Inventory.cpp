#include <algorithm>
#include "Inventory.h"

// Constructor
Inventory::Inventory() {}

// Function to add an item to the inventory
void Inventory::AddItem(const Item& item) {
    items.push_back(item);
}

// Function to remove an item from the inventory
void Inventory::RemoveItem(const Item& item) {
   items.erase(std::remove(items.begin(), items.end(), item), items.end());

}

// Function to check if the inventory contains a specific item
bool Inventory::HasItem(const Item& item) const {
  return std::find(items.begin(), items.end(), item) != items.end();


}

// Function to get all items in the inventory
const std::vector<Item>& Inventory::GetItems() const {
    return items;
}
