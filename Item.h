// Item.h
#ifndef ITEM_H
#define ITEM_H

#include <string>

class Item {
private:
    std::string name;
    std::string description;

public:
    // Constructor
    Item(const std::string& itemName, const std::string& itemDescription);

   Item& operator=(const Item& other) {
       if (this != &other) {
           name = other.name;
           description = other.description;
       }
       return *this;
   }


    // Function to describe the interaction with the item
    void Interact() const;

   // Function to get the item's description
   const std::string& GetDescription() const;

    // Getter for name
    const std::string& GetName() const;

    // Overloading == operator to compare items
    bool operator==(const Item& other) const;
};

#endif
