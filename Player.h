// Player.h
#ifndef PLAYER_H
#define PLAYER_H

#include "Character.h"
#include "Item.h"
#include <vector>

// Forward declaration
class Room;

class Player : public Character {
private:
    Room* location;
    std::vector<Item> inventory; // Modify to store items in the player's inventory
    size_t inventorySize; // New member variable to track inventory size

public:
    // Constructor
    Player(const std::string& name, int health);

    // Function to set player's location
    void SetLocation(Room* newLocation);

    // Function to get player's location
    Room* GetLocation() const;

    // Function to add an item to the player's inventory
    bool AddToInventory(const Item& item);

    // Function to remove an item from the player's inventory
    void RemoveFromInventory(const Item& item);

    // Function to interact with an item in the player's inventory
    void InteractWithInventoryItem(const std::string& itemName);

    // Function to display player's inventory
    void DisplayInventory() const;

    // Function to check if the player's inventory is full
    bool InventoryIsFull() const;

    // Function to get the player's inventory
    const std::vector<Item>& GetInventory() const;
};

#endif
