#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <vector>
#include <map>
#include "Item.h"
#include "Player.h"
#include "Chest.h" 

class Room {
private:
    std::string name;
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;
    Chest chest; // Include a Chest instance

public:
    // Constructor
    Room(const std::string& name, const std::string& desc);

    
    void AddItem(const Item& item);

    
    void RemoveItem(const Item& item);

   
    void AddExit(const std::string& direction, Room* room);

    // Function to get the room's name
    const std::string& GetName() const;

    // Function to get the room's description
    const std::string& GetDescription() const;

    // Function to interact with an item in the room
    void InteractWithItem(const std::string& itemName, Player& player);

    // Function to interact with the chest in the room
    void InteractWithChest(Player& player);

    // Function to get an exit
    Room* GetExit(const std::string& direction);

    // Function to print available exits
    void PrintAvailableExits() const;

    // Getters for items and exits
    const std::vector<Item>& GetItems() const;
    const std::map<std::string, Room*>& GetExits() const;
};

#endif
